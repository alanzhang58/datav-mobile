import request from '../utils/request'

export const getMobileData = () => {
  return request({
    url: '/screen/mobile',
    method: 'get'
  })
}

export const getMockData = () => {
  return request({
    url: '/screen/mock',
    method: 'get'
  })
}
