import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as Echarts from 'echarts'
import VueEcharts from 'vue-echarts'
import '@/style/reset.css'

Vue.component('vue-echarts', VueEcharts)

Vue.config.productionTip = false

Vue.prototype.$echarts = Echarts
Vue.prototype.$bmap = window.BMapGL
Vue.prototype.$initMap = window.initMap
Vue.prototype.$mapvgl = window.mapvgl
Vue.prototype.$mapv = window.mapv
Vue.prototype.$purpleStyle = window.purpleStyle

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
